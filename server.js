const
  express = require ('express'),
  app = express(),
  port = 3001;
  var dateFormat = require ("dateformat");
 

var 
	fs = require ('fs');
  exec = require ('child_process').exec;
  bodyParser = require ('body-parser');

app.use(bodyParser());

//ENDPOINTS

//javascript sources

app.get ('/edc/pembayaran_poin/new.xml', function (req,res) {
  console.log (req.headers);
  console.log (req.query);
  res.redirect ('/public/poinBayar.xml');
  res.end();
});

app.get ('/edc/report_transaksi_cashback/new.xml', function (req,res) {
  console.log (req.headers);
  console.log (req.query);
  res.redirect ('/public/poinReport.xml');
  res.end();
});

app.get ('/edc/initial_time/new', function (req,res) {
  console.log (req.headers);
  console.log (req.query);
  res.send (dateFormat (Date(), "yyyy-mm-dd HH:MM"));
});

app.get ('/edc/pendaftaran_kartu/new.xml', function (req,res) {
  console.log (req.headers);
  console.log (req.query);
  res.redirect ('/public/poinDaftar.xml');
  res.end();
});

app.use ('/media', express.static('media'));
app.use ('/public', express.static('public'));

app.listen (port, function (err) {
  console.log ('Listening on port ' + port);
});
